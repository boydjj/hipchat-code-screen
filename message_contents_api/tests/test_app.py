"""
End-to-end tests.

Note: these require network access.
"""
import json
import logging
import urllib.parse
from unittest import mock

from tornado import testing

from .. import app
from .utils import setup_fetch

logger = logging.getLogger(__name__)


class MessageDetailsTestCase(testing.AsyncHTTPTestCase):
    # Utility methods:
    def _get_api_response(self, message):
        quoted_message = urllib.parse.quote_plus(message)
        return self.fetch('/message-contents/?message={}'.format(quoted_message))

    def check_api_response(self, message, expected=None, status=200, check_body=True):
        response = self._get_api_response(message)
        self.assertEqual(response.code, status)

        if check_body:
            body = json.loads(response.body.decode('utf-8'))
            self.assertEqual(body, expected)

    def get_app(self):
        return app.make_app()

    # Actual tests:
    def test_no_extra_contents_204(self):
        message = 'hello, world!'
        self.check_api_response(message, status=204, check_body=False)

    def test_no_message_400(self):
        response = self.fetch('/message-contents/')
        self.assertEqual(response.code, 400)

    def test_given_case_1(self):
        message = '@chris you around?'
        expected = {
            'mentions': [
                'chris',
            ]
        }
        self.check_api_response(message, expected)

    def test_given_case_2(self):
        message = 'Good morning! (megusta) (coffee)'
        expected = {
            'emoticons': [
                'megusta',
                'coffee'
            ]
        }
        self.check_api_response(message, expected)

    def test_given_case_3(self):
        message = 'Olympics are starting soon; http://www.nbcolympics.com'
        expected_title = '2016 Rio Olympic Games | NBC Olympics'
        expected = {
            'links': [
                {
                    'url': 'http://www.nbcolympics.com',
                    'title': expected_title
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<title>{}</title>'.format(expected_title)
            )
            self.check_api_response(message, expected)

    def test_given_case_4(self):
        message = '@bob @john (success) such a cool feature; ' \
                  'https://twitter.com/jdorfman/status/430511497475670016'
        expected_title = 'Justin Dorfman on Twitter: &quot;nice @littlebigdetail from ' \
                         '@HipChat (shows hex colors when pasted in chat). ' \
                         'http://t.co/7cI6Gjy5pq&quot;'
        expected = {
            'mentions': [
                'bob',
                'john'
            ],
            'emoticons': [
                'success'
            ],
            'links': [
                {
                    'url': 'https://twitter.com/jdorfman/status/430511497475670016',
                    'title': expected_title
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<title>{}</title>'.format(expected_title)
            )
            self.check_api_response(message, expected)

    def test_unicode_chars(self):
        message = 'Square-Enix\'s Japanese site is at www.jp.square-enix.com'
        expected_title = 'スクウェア・エニックス 商品・サービス・企業情報 | SQUARE ENIX'
        expected = {
            'links': [
                {
                    'url': 'www.jp.square-enix.com',
                    'title': expected_title,
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<title>{}</title>'.format(expected_title)
            )
            self.check_api_response(message, expected)
        self.check_api_response(message, expected)

    def test_follows_redirect(self):
        message = 'check out our app at https://app.experimentengine.com/'
        expected_title = 'Experiment Engine | Login'
        expected = {
            'links': [
                {
                    'url': 'https://app.experimentengine.com/',
                    'title': expected_title
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<title>{}</title>'.format(expected_title)
            )
            self.check_api_response(message, expected)

    def test_no_title_if_400(self):
        message = 'httpbin.org/status/400 returns a 400'
        expected = {
            'links': [
                {
                    'url': 'httpbin.org/status/400'
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(mock_get_client.return_value.fetch, 400, '')
            self.check_api_response(message, expected)

    def test_no_title_if_503(self):
        message = 'httpbin.org/status/503 returns a 503'
        expected = {
            'links': [
                {
                    'url': 'httpbin.org/status/503'
                }
            ]
        }
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(mock_get_client.return_value.fetch, 503, '')
            self.check_api_response(message, expected)

