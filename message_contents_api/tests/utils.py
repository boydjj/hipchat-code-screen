import io

from tornado.concurrent import Future
from tornado.httpclient import HTTPRequest, HTTPResponse


def setup_fetch(fetch_mock, status_code, body):
    """
    Given a mock of a method (generally `AsyncHTTPClient.fetch`), create a response from
    the given status code and body and set the method's return value to a future with the
    response as a result.

    Slightly modified from https://gist.github.com/fungusakafungus/6336801
    """
    # TODO: figure out how to return a list of futures to support multiple calls
    if body is not None:
        # UTF-8 is the default _get_content_charset uses.
        body = body.encode('utf-8')

    def side_effect(request, **kwargs):
        request = HTTPRequest(request)
        buffer = io.BytesIO(body)
        response = HTTPResponse(request, status_code, None, buffer)
        future = Future()
        future.set_result(response)
        return future

    fetch_mock.side_effect = side_effect
