import logging
import unittest

from . import regex

logger = logging.getLogger(__name__)


class EmoticonRegexpTestCase(unittest.TestCase):
    def test_no_mentions(self):
        s = "hello, world!"
        expected = []
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_starting_string(self):
        s = '@bob you here?'
        expected = ['bob']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_middle_and_eos(self):
        s = "Good morning! @jeremy @lydia"
        expected = ['jeremy', 'lydia']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_appearing_after_newline(self):
        s = 'hey\n@jeremy how is your day going?'
        expected = ['jeremy']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_start_parens(self):
        s = 'mentions can (@start a parenthetical)'
        expected = ['start']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_end_parens(self):
        s = 'mentions can also (be at the @end)'
        expected = ['end']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_exclude_in_url(self):
        s = 'this guy really doesn\'t tweet much: https://twitter.com/@jeremyjboyd'
        expected = []
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_exclude_embedded(self):
        s = 'this mention is em@bedded in a word'
        expected = []
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)

    def test_all_cases_together(self):
        s = (
            "@bob @chris here is a tweet: twitter.com/@jeremyjboyd. oh and @chris@jeremy "
            "only one of you should be mentioned.\n@linder and @lydia are@you matched on "
            "this new line? what about mytweet.@jeremy? did we mention @jeremy? This "
            "looks decent. we can also parenthesize a (@mention) and some @mentions42 "
            "have numbers. at the end of the string we ping @shannon"
        )
        expected = ['bob', 'chris', 'chris', 'linder', 'lydia', 'jeremy', 'mention',
                    'mentions42', 'shannon']
        self.assertEqual(regex.MENTIONS_RE.findall(s), expected)
