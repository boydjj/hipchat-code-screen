import logging
import re

logger = logging.getLogger(__name__)

# Spec:
# - "Always starts with an '@' and ends when hitting a non-word character."
# - "We do not handle mentions inside a URL or inside any word"

# Notes:
# - We use lookaheads/lookbehinds here because non-word characters such as '/' in a URL
#   will match '\b' on the left side.
MENTIONS_RE = re.compile(
    r'(?=(?<=^)|(?<=\s)|(?<=\())@'  # preceded by whitespace, ^, or (
    r'(\w+)'  # capture all the word chars following the parens
    r'\b'  # finish with a )
)
