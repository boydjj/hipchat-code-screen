import cgi
import logging
import re
import socket

from tornado import gen
from tornado import httpclient

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class LinkParser:
    """
    Contains all functionality for parsing links out of a message.

    To add handling for new link types:
        - create a new subclass of BaseLinkHandler
        - register it with the `link_parser` instance of LinkParser created below

    See `FTPLinkHandler` and `MailtoHandler` for  examples of adding handlers for non-HTTP
    URLs. Customization is easy -- e.g., we could  add an 'email' property in
    `MailtoHandler.handle` if the client needed it.
    """
    def __init__(self):
        self.handlers = {}

    def register(self, handler_cls):
        if handler_cls not in self.handlers:
            self.handlers[handler_cls] = handler_cls()

    @gen.coroutine
    def get_links_from_message(self, message):
        """
        Check each 'word' in a message for whether it's a link and handle it accordingly.
        :param message: a message of type `str`
        :return: a list of dictionaries of all links and their metadata, if applicable
        """
        tokens = [m.lower() for m in message.split()]
        handled_tokens = {}
        links = []

        for token in tokens:
            if token in handled_tokens:
                if handled_tokens[token] is not None:
                    links.append(handled_tokens[token])
                continue
            for handler_cls, handler_instance in self.handlers.items():
                if handler_instance.identify(token):
                    link = yield handler_instance.handle(token)
                    handled_tokens[token] = link
                    links.append(link)
                    break
            else:
                # Cache that we couldn't find a handler
                handled_tokens[token] = None

        return links


class BaseLinkHandler:
    def identify(self, token):
        """
        Returns True if this class identifies `token` as a URL it can handle. All
        subclasses must define this method.
        """
        raise NotImplementedError()

    @gen.coroutine
    def handle(self, url):
        """
        Return a dictionary representing the link. Any overrides must be coroutines.

        If we don't know of any metadata for a URL, we don't return anything extra.
        """
        return {
            'url': url,
        }


class HTTPLinkHandler(BaseLinkHandler):
    # There are a ton of regexes out there that purport to recognize URLs in real-world
    # text. I can find none, however, that manage to handle each of the cases I wanted to
    # highlight here. This one is modified slightly from
    # https://gist.github.com/dperini/729294#gistcomment-1296121 and does the trick.
    # See `utils.links.tests.http_regex` for some examples.
    HTTP_RE = re.compile(
        r'^'
        # scheme
        r'(?:https?://)?'
        r'(?:'
            # IP address exclusion
            # private & local networks
            r'(?!(?:10|127)(?:\.\d{1,3}){3})'
            r'(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})'
            r'(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})'
            # IP address dotted notation octets
            # excludes loopback network 0.0.0.0
            # excludes reserved space >= 224.0.0.0
            # excludes network & broadcast addresses
            # (first & last IP address of each class)
            r'(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])'
            r'(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}'
            r'(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))'
        r'|'
            # host name
            r'(?:(?:[A-Za-z\u00a1-\uffff0-9]-?)*[a-z\u00a1-\uffff0-9]+)'
            # domain name
            r'(?:\.(?:[A-Za-z\u00a1-\uffff0-9]-?)*[a-z\u00a1-\uffff0-9]+)*'
            # TLD identifier
            r'(?:\.(?:[A-Za-z\u00a1-\uffff]{2,}))'
        r')'
        # port number
        r'(?::\d{2,5})?'
        # resource path
        r'(?:/\S*)?'
        r'$'
    )

    def identify(self, token):
        # This is a kludgy way to identify whether a token is an HTTP URL. A more robust
        # approach should
        return bool(self.HTTP_RE.match(token))

    def _get_content_charset(self, response, default='utf-8'):
        """
        Attempt to get the charset out of a Content-Type header.
        See https://github.com/tornadoweb/tornado/pull/776#issuecomment-69490421
        """
        # cgi.parse_header implicitly converts to str, so we force it here
        content_type_header = str(response.headers.get('Content-Type'))
        content_type, params = cgi.parse_header(content_type_header)
        return params.get('charset', default)

    def _get_client(self):
        return httpclient.AsyncHTTPClient()

    @gen.coroutine
    def handle(self, url):
        logger.debug('Fetching %s', url)
        http_client = self._get_client()
        result = {'url': url}

        if not url.startswith('http://') and not url.startswith('https://'):
            url = 'http://' + url

        try:
            response = yield http_client.fetch(url, connect_timeout=2, request_timeout=2)
        except (httpclient.HTTPError, socket.gaierror):
            # Could not retrieve the page
            pass
        else:
            body = response.body.decode(self._get_content_charset(response))

            # TODO: avoid Lovecraftian nightmares by using BeautifulSoup or lxml
            TITLE_RE = re.compile(r'<title>(.+)</title>')

            # The internet is full of terrible HTML; combine every `title` we find
            titles = re.findall(TITLE_RE, body)
            if titles:
                result.update({'title': titles[0]})

        return result


class FTPLinkHandler(BaseLinkHandler):
    FTP_RE = re.compile(r'\b(ftp://[\w./]+\.[\w./]+)\b')

    def identify(self, token):
        return bool(self.FTP_RE.match(token))

    @gen.coroutine
    def handle(self, url):
        return {
            'url': self.FTP_RE.match(url).group()
        }


class MailtoHandler(BaseLinkHandler):
    MAILTO_RE = re.compile(r'\b(mailto:[\w.]+@[\w-]+\.[\w]+)\b')

    def identify(self, token):
        return bool(self.MAILTO_RE.match(token))

    @gen.coroutine
    def handle(self, url):
        return {
            'url': self.MAILTO_RE.match(url).group()
        }

link_parser = LinkParser()
link_parser.register(HTTPLinkHandler)
link_parser.register(FTPLinkHandler)
link_parser.register(MailtoHandler)
