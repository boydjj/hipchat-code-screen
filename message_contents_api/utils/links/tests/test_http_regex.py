import logging
import unittest

from ..links import HTTPLinkHandler

logger = logging.getLogger(__name__)


class HTTPRegexTestCase(unittest.TestCase):
    def test_no_urls(self):
        s = 'hello'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = []
        self.assertEqual(actual, expected)

    def test_no_scheme(self):
        s = 'google.com'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)

    def test_no_scheme_with_path_and_query(self):
        s = 'example.com/foo.bar?param1=hello&param2=world'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)

    def test_http_scheme(self):
        s = 'http://example.com/foo.bar?param1=hello&param2=world'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)

    def test_https_scheme(self):
        s = 'https://example.com/foo.bar?param1=hello&param2=world'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)

    def test_new_tld(self):
        s = 'http://tree.house/hiworld.html'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)

    def test_localhost_excluded(self):
        s = '127.0.0.1'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = []
        self.assertEqual(actual, expected)

        s = 'http://localhost/'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = []
        self.assertEqual(actual, expected)

    def test_no_scheme_four_part(self):
        s = 'www.jp.square-enix.com'
        actual = HTTPLinkHandler.HTTP_RE.findall(s)
        expected = [s]
        self.assertEqual(actual, expected)
