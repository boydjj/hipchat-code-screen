import logging
import unittest
from unittest import mock

from tornado.testing import AsyncTestCase
from tornado.testing import gen_test

from message_contents_api.tests.utils import setup_fetch

from .. import links

logger = logging.getLogger(__name__)


class LinkParserTestCase(unittest.TestCase):
    """
    Test registry functionality of LinkParser
    """
    def test_register_saves_handler(self):
        lp = links.LinkParser()
        lp.register(links.FTPLinkHandler)
        self.assertIn(links.FTPLinkHandler, lp.handlers)

    def test_register_does_not_dupe_handler(self):
        lp = links.LinkParser()
        lp.register(links.FTPLinkHandler)
        self.assertIn(links.FTPLinkHandler, lp.handlers)

        lp.register(links.FTPLinkHandler)
        self.assertEqual(len(lp.handlers), 1)


class LinkHandlerTestCase(AsyncTestCase):
    @gen_test
    def test_no_links(self):
        message = 'hello, world!'
        expected = []
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)

    @gen_test
    def test_ftp_links(self):
        message = "Hey, go download this file: ftp://ftp.example.com/foo.zip\n" \
                  "ftp://ftp.example.com/bar.zip is cool, too\n" \
                  "how about ftp://ftp.example.com/baz.zip, friend?"
        expected = [
            {'url': 'ftp://ftp.example.com/foo.zip'},
            {'url': 'ftp://ftp.example.com/bar.zip'},
            {'url': 'ftp://ftp.example.com/baz.zip'},
        ]
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)

    @gen_test
    def test_mailto_links(self):
        message = "mailto:foo@example.com, mailto:bar@example-domain.com\n" \
                  "mailto:jeremy\n" \
                  "how about mailto:jeremy@foo.com, friend?\n" \
                  "mailto:bar@example-domain2.com is one, and so is " \
                  "mailto:baz@example.com"
        expected = [
            {'url': 'mailto:foo@example.com'},
            {'url': 'mailto:bar@example-domain.com'},
            {'url': 'mailto:jeremy@foo.com'},
            {'url': 'mailto:bar@example-domain2.com'},
            {'url': 'mailto:baz@example.com'},
        ]
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)

    @gen_test
    def test_http_links(self):
        message = "hey take a look at google.com man"
        expected = [
            {
                'url': 'google.com',
                'title': 'Google'
            }
        ]
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()
            setup_fetch(mock_get_client.return_value.fetch, 200, '<title>Google</title>')
            actual = yield links.link_parser.get_links_from_message(message)
            self.assertEqual(actual, expected)

    @gen_test
    def test_unicode_title(self):
        message = "one of my favorite game companies: http://www.jp.square-enix.com/"
        expected = [
            {
                'url': 'http://www.jp.square-enix.com/',
                'title': 'スクウェア・エニックス 商品・サービス・企業情報 | SQUARE ENIX'
            }
        ]
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<title>スクウェア・エニックス 商品・サービス・企業情報 | SQUARE ENIX</title>'
            )
            actual = yield links.link_parser.get_links_from_message(message)
            self.assertEqual(actual, expected)

    @gen_test
    def test_no_title(self):
        message = "one of my favorite game companies: http://www.jp.square-enix.com/"
        expected = [
            {
                'url': 'http://www.jp.square-enix.com/',
            }
        ]
        patch_dest = 'message_contents_api.utils.links.links.HTTPLinkHandler._get_client'
        with mock.patch(patch_dest) as mock_get_client:
            mock_get_client.return_value = mock.MagicMock()

            setup_fetch(
                mock_get_client.return_value.fetch, 200,
                '<head></head><body>hello world</body>'
            )
            actual = yield links.link_parser.get_links_from_message(message)
            self.assertEqual(actual, expected)

    @gen_test
    def test_multiple_http_links(self):
        message = 'using https://google.com/ i found SE\'s website at ' \
                  'www.jp.square-enix.com'
        expected = [
            {
                'url': 'https://google.com/',
                'title': 'Google'
            },
            {
                'url': 'www.jp.square-enix.com',
                'title': 'スクウェア・エニックス 商品・サービス・企業情報 | SQUARE ENIX'
            }
        ]
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)

    @gen_test
    def test_multiple_link_types(self):
        message = 'hey here is ftp://ftp.example.com/foo.bar and google.com and ' \
                  'ftp.example.com you can send an email to mailto:boydjj@gmail.com'
        expected = [
            {
                'url': 'ftp://ftp.example.com/foo.bar'
            },
            {
                'url': 'google.com',
                'title': 'Google'
            },
            {
                'url': 'ftp.example.com'
            },
            {
                'url': 'mailto:boydjj@gmail.com'
            }
        ]
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)

    @gen_test
    def test_multiple_instances_of_same_link(self):
        message = 'go to ftp://ftp.example.com/foo.bar and ftp://ftp.example.com/foo.bar'
        expected = [
            {
                'url': 'ftp://ftp.example.com/foo.bar',
            },
            {
                'url': 'ftp://ftp.example.com/foo.bar',
            },
        ]
        actual = yield links.link_parser.get_links_from_message(message)
        self.assertEqual(actual, expected)
