import logging
import re

logger = logging.getLogger(__name__)

__all__ = ['EMOTICON_RE',]

# Spec:
# "you only need to consider 'custom' emoticons which are alphanumeric strings, no longer
# than 15 characters, contained in parenthesis"
# Notes:
# - I'm assuming we aren't interested in any emoticons occurring inside URLs, similar to
#   mentions.
EMOTICON_RE = re.compile(
    r'(?=(?<=\s)|(?<=^)|(?<=\())\('  # must be preceded by whitespace, ^, or (
    r'(\w{1,15})'  # capture the 1-15 word characters inside the parentheses
    r'\)'  # finish with a )
)
