import logging
import unittest

from . import regex

logger = logging.getLogger(__name__)


class EmoticonRegexpTestCase(unittest.TestCase):
    def test_no_emoticons(self):
        s = "hello, world!"
        expected = []
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_starting_string(self):
        s = '(megusta) that coffee'
        expected = ['megusta']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_middle_and_eos(self):
        s = "Good morning! (megusta) (coffee)"
        expected = ['megusta', 'coffee']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_appearing_after_newline(self):
        s = 'hey jeremy\n(smiley) how is your day going?'
        expected = ['smiley']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_start_parens(self):
        s = 'emoticons can ((start) a parenthetical)'
        expected = ['start']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_end_parens(self):
        s = 'emoticons can also (be at the (end))'
        expected = ['end']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_exclude_in_url(self):
        s = 'https://en.wikipedia.org/wiki/Disambiguation_(disambiguation).html'
        expected = []
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_exclude_long_emoticons(self):
        s = 'plz exclude: (reallylongemojisthatreallyshouldnotexist) kthx'
        expected = []
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_exclude_embedded(self):
        s = 'this emoticon is em(bedde)d in a word'
        expected = []
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)

    def test_all_cases_together(self):
        s = (
            "(awyiss) <-- this is an emoticon. Don't match emoticons em(bedde)d in a "
            "word, but do (match) (others). Don't match parentheticals appearing in URLs "
            "like http://www.google.com/?q=(megusta) or https://en.wikipedia.org/wiki/"
            "Disambiguation_(disambiguation).html (which is not a real thing)\n"
            "(emoticons) on a new line are okay. they can end a(parenthetical "
            "(statement)) or ((can) start one). "
            "(reallylongemojisthatreallyshouldnotexist) should not match. and emoticons "
            "can end a (string)"
        )
        expected = ['awyiss', 'match', 'others', 'emoticons', 'statement', 'can', 'string']
        self.assertEqual(regex.EMOTICON_RE.findall(s), expected)
