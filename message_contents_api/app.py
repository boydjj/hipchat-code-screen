import logging

from tornado import gen
from tornado import ioloop
from tornado import options
from tornado import web

from .utils.links import links as links_utils
from .utils.emoticons import regex as emoticon_regex
from .utils.mentions import regex as mentions_regex

options.define('port', default=8888, help='Port to listen on', type=int)
options.define('debug', default=False, help='Whether to run the app in debug mode',
               type=bool)

logger = logging.getLogger(__name__)


class MessageContentsHandler(web.RequestHandler):
    @gen.coroutine
    def get(self):
        message = self.get_argument('message')
        logger.debug('Received message: %s', message)

        result = {}

        # See README.md for discussion of these different handling mechanisms
        mentions = mentions_regex.MENTIONS_RE.findall(message)
        emoticons = emoticon_regex.EMOTICON_RE.findall(message)
        links = yield links_utils.link_parser.get_links_from_message(message)

        if any((mentions, emoticons, links)):
            if mentions:
                result['mentions'] = mentions
            if emoticons:
                result['emoticons'] = emoticons
            if links:
                result['links'] = links
            self.write(result)
        else:
            self.set_status(204)


def make_app():
    return web.Application(
        [
            (r'/message-contents/', MessageContentsHandler),
        ],
        debug=options.options.debug
    )


def main():
    options.parse_command_line()
    app = make_app()
    logger.info('Listening on port: %s', options.options.port)
    logger.info('Debug is set to: %s', options.options.debug)
    app.listen(options.options.port)
    ioloop.IOLoop.current().start()

if __name__ == '__main__':
    main()
