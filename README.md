# HipChat Code Screen

This repository is my answer to the HipChat code screen from Atlassian. Details of the
requirements are available in `code_screen.txt`.

## Running the code

This code was written to be executed with Python 3.5.2 and makes use of Python 3.x
features (such as `return` from within a generator).

To set up locally, create a `virtualenv` with a Python 3.5 interpreter and then execute:

    pip install -r requirements.txt

### Tests

Coverage on the package is relatively high. `pytest` is included as a dependency, as well as `coverage`, and you can take a look at the results with `py.test --cov=message_contents_api --cov-config=.coveragerc --cov-report=html`.

Not all network access is properly mocked out in the test suite, so there are intermittent failures. See **TODO** for the steps we need to take to fix this. In the meantime, re-running tests generally leads to all tests passing.

**Note:** If you see a `SyntaxError` such as `SyntaxError: 'return' with argument inside generator (links.py, line 46)`, you may not have activated your virtualenv.

## API

Note: all non-empty responses are JSON-encoded.

### `GET /message-contents/?message=<message>`

Returns message contents per the requirements. Messages are encoded as query parameters and not in the request body to enable caching beyond the application layer. See Roy Fielding's [comment] on this for more  information. The value of `message` must be properly [percent-encoded].

[comment]: https://groups.yahoo.com/neo/groups/rest-discuss/conversations/messages/9962
[percent-encoded]: https://developer.mozilla.org/en-US/docs/Glossary/percent-encoding

#### Response structure

##### Status

 * `200`: message contents found; see **Body** below
 * `204`: no message contents found; response body empty
 * `400`: no `message` parameter in request

##### Body

    {
      "mentions": [
          "<mention1>",
          "<mention2>",
          ...
      ],
      "emoticons": [
          "<emoticon1>"
          ...
      ],
      "links": [
          {
              "url": "<HTTP URL>",
              "title": "<title of page at url>"
          },
          {
              "url": "<non-HTTP (e.g., FTP) URL>",
          },
          ...
      ]
    }

Notes: 

1. Each of the `mentions`, `emoticons`, and `links` arrays will only be present in the response if they are not empty.
1. Any element in an array may be repeated if it occurs more than once in the message. E.g., `mentions` may be `["bob", "bob"]`. 
1. Currently, the only supported URL types are HTTP, FTP, and mailto. 
1. Only HTTP links have `title` properties. If an HTTP link is irretrievable (e.g., if it returns a 400 or 503), the API will still return the link but no `title`.

## Architecture discussion

### Types of elements found in a message

Finding emoticons and mentions differs fundamentally from finding links because we need to *act* on
the links that we find. As a result, they're handled differently in the `utils` package.

Emoticons and mentions are pretty simple exercises in regex building. Links are that plus identifying 
the *kind* of URL that we've found in the text -- and then doing something with that information. Since
URLs [should not have spaces], the easiest way to handle this is to tokenize the message and then 
evaluate each token in turn. That's why I have the `LinkParser` and link handler registry system in 
`utils.links`.

[should not have spaces]: http://stackoverflow.com/a/497972/102467

### TODO

1. **Mock multiple calls to `fetch`**: Executions that call `AsyncHTTPClient.fetch` more than once still need to be mocked out. It's not currently clear to me how to return multiple values from a generator. 
1. **Mock connection/request timeouts**: `HTMLLinkHandler` uses these and falls back to returning no title.
1. **Edge cases:** I'm sure I missed *something*.
